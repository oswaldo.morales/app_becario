from django.db import models
from django.contrib.postgres.fields import JSONField

# Create your models here.

class Aspirants(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    mail = models.CharField(max_length=100, blank=True, null=True)
    mobile = models.CharField(max_length=10, blank=True, null=True)
    country = models.CharField(max_length=85, blank=True, null=True)
    age = models.IntegerField(default=0)
    # currentStatusId = 
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)
    updatedAt = models.DateField(blank=True, null=True)
    updatedBy = models.CharField(max_length=50, blank=True, null=True)
    deletedAt = models.DateField(blank=True, null=True)
    deletedBy = models.CharField(max_length=50, blank=True, null=True)


class AspirantStatusLogs(models.Model):
    aspirantId = models.ForeignKey(Aspirants, on_delete=models.CASCADE)
    # statusId = 
    active = models.BooleanField(default=True)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)


class AspirantSurveys(models.Model):
    aspirantId = models.ForeignKey(Aspirants, on_delete=models.CASCADE)
    # surveyId = models.ForeignKey(Aspirants)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)


class AspirantComments(models.Model):
    aspirantId = models.ForeignKey(Aspirants, on_delete=models.CASCADE)
    # userId = 
    comment = models.CharField(max_length=300, blank=True, null=True)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)
    updatedAt = models.DateField(blank=True, null=True)
    updatedBy = models.CharField(max_length=50, blank=True, null=True)
    deletedAt = models.DateField(blank=True, null=True)
    deletedBy = models.CharField(max_length=50, blank=True, null=True)

class AspirantLogs(models.Model):
    aspirantId = models.ForeignKey(Aspirants, on_delete=models.CASCADE)
    entityType = models.CharField(max_length=50, blank=True, null=True)
    # entityId = 
    action = models.CharField(max_length=50, blank=True, null=True) 
    message = models.CharField(max_length=100, blank=True, null=True)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)