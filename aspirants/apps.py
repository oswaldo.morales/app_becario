from django.apps import AppConfig


class AspirantsConfig(AppConfig):
    name = 'aspirants'
