from django.urls import path
from . import views2

# from .views import FileView
# from .views import FormInputTypeView
# from .views import SurveyAnswerView
# from .views import SurveyFormView
# from .views import SystemLogViews
from . import handler

urlpatterns = [
    path('', views2.index, name="survey"),
    path('question/', views2.question, name="question"),
    path('render/', handler.renderFormQuestions, name="render"),
    path('render/multi/', handler.renderFormMultiQuestions, name="renderMulti"),
    # path('crear/', SurveyFormView, name="createSurvey"),
    # path('editar/', SurveyFormView, name="detailSurvey"),
    # path('eliminar/', SurveyFormView, name="deleteSurvey"),
]
