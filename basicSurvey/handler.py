from django.shortcuts import render

def saveFormQuestionsJson():
    pass

openQuestionJson = {
    'type': 'openQuestion',
    'nameQuestion': 'questions[0][question]',
    'name': 'questions[0][open]',
    'question': '¿Describete en una frase?',#value
}

def renderFormQuestions(request):
    #Crear pregunta abierta
    formOpenQuestion = f'''
    <div class="row">
        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
            <div class="form-row">
                <div class="col-md-12 mb-4">
                    <label for="fullName">Pregunta abierta</label>
                    <input type="hidden" name="{openQuestionJson.get('name')}">
                    <input type="text" name="{openQuestionJson.get('nameQuestion')}" value="" class="form-control" id="PreguntaAbierta">
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                    <div class="invalid-feedback">
                        Please fill the name
                    </div>
                </div>
            </div>
        </div>
    </div> '''
    
    return render(request, 'basicSurvey/surveyDetail.html', {'formOpenQuestion': formOpenQuestion})

multiQuestionJson = {
    "type": 'multiQuestion',
    'nameQuestion': 'questions[0][question]',
    'name': '',
    'question': 'Grado de estudio',
    'typeSelection': 'select,dropDown..',
    'options':[
            {
                'labe': 'Preparatoria',
                'value': 'Preparatoria',  
            },
            {
                'labe': 'Universidad',
                'value': 'Universidad',  
            },
            {
                'labe': 'Posgrado',
                'value': 'Posgrado',  
            },
            {
                'labe': '',
                'value': '',  
            },
            {
                'labe': '',
                'value': '',  
            },
            
        ],
}

def renderFormMultiQuestions(request):
    
    formMultiQuestion = '''
    <div class="form-row">
        <div class="col-md-12 mb-4">
        
            <label for="fullName">Pregunta Opción Multiple</label>
            <input type="text" name="" class="form-control" id="PreguntaOpcionMultiple" placeholder="Pregunta Opción Multiple" value="" required>
            <input type="hidden" name="" value="">

            
            <label for="fullName">Opción 1</label>
            <input type="text" class="form-control" id="Opcion1" placeholder="Opción 1" value="" required>

            <label for="fullName">Opción 2</label>
            <input type="text" class="form-control" id="Opcion2" placeholder="Opción 2" value="" required>
            
            <label for="fullName">Opción 3</label>
            <input type="text" class="form-control" id="Opcion3" placeholder="Opción 3" value="" required>
            
            <label for="fullName">Opción 4</label>
            <input type="text" class="form-control" id="Opcion4" placeholder="Opción 4" value="" required>
            
            <label for="fullName">Opción 5</label>
            <input type="text" class="form-control" id="Opcion5" placeholder="Opción 5" value="" required>
            
            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please fill the name
            </div>
        </div>
    </div>''' 
    
    return render(request, 'basicSurvey/surveyDetail.html', {'formMultiQuestion': formMultiQuestion})


#dudas
scoreQuestionJson = {
    'type': 'scoreQuestion',
    'nameQuestion': 'Del 1 al 5 que tanto sabes de Java, donde 1 es poco y 5 mucha',
    'options':[
            {
                'labe': 'Minimo',
                'value': '1',  
            },
            {
                'labe': 'Poco',
                'value': '2',  
            },
            {
                'labe': 'Basico',
                'value': '3',  
            },
            {
                'labe': 'Intermedio',
                'value': '4',  
            },
            {
                'labe': 'Mucha',
                'value': '5',  
            },
        ],
}

def renderFormScoreQuestions(request):
    formScoreQuestion = '''
    <div class="form-row">
        <div class="col-md-12 mb-4">
            <label for="fullName">Pregunta tipo Calificación</label>
            <input type="text" class="form-control" id="PreguntaDeCalificacion" placeholder="Pregunta tipo Calificación" value="" required>
            
            <label for="fullName">Calificación Mínima</label>
            <input type="text" class="form-control" id="CalificacionMinima" placeholder="Calificación Mínima" value="" required>
            
            <label for="fullName">Calificación Máxima</label>
            <input type="text" class="form-control" id="CalificacionMaxima" placeholder="Calificación Máxima" value="" required>

            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please fill the name
            </div>
        </div>
    </div>'''
    pass
   
fileQuestionJson = {
    'type': 'fileQuestion',
    'nameQuestion':''
}
   
def renderFormFileQuestion(request):
    formFileQuestion = '''
    <div class="form-row">
        <div class="col-md-12 mb-4">
            
            <label for="fullName">Descripción de archivo</label>
            <input type="text" class="form-control" id="Descripcionarchivo" placeholder="Descripción de archivo" value="" required>
            
            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please fill the name
            </div>
        </div>
    
    
        <div class="col-md-12 mb-4">
            
            <label for="fullName">Tamaño del archivo</label>
            <input type="text" class="form-control" id="Descripcionarchivo" placeholder="2mb" value="" required>
            
            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please fill the name
            </div>
        </div>
    
        <div class="col-md-12 mb-4">
            
            <label for="fullName">Tipo de archivo</label>
            
            <div class="btn-group mb-4 mr-2" role="group">
                <button id="btnOutline" type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PDF<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></button>
                <div class="dropdown-menu" aria-labelledby="btnOutline">
                    <a href="javascript:void(0);" class="dropdown-item"><i class="flaticon-home-fill-1 mr-1"></i>XML</a>
                    <a href="javascript:void(0);" class="dropdown-item"><i class="flaticon-gear-fill mr-1"></i>PNG</a>
                    <a href="javascript:void(0);" class="dropdown-item"><i class="flaticon-gear-fill mr-1"></i>PDF</a>
    
                </div>
            </div>
            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please fill the name
            </div>
        </div>
        <div class="col-md-12 mb-4">
            
            <label for="fullName">Guardar en:</label>
            
            <div class="btn-group mb-4 mr-2" role="group">
                <button id="btnOutline" type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">S3<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></button>
                <div class="dropdown-menu" aria-labelledby="btnOutline">
                    <a href="javascript:void(0);" class="dropdown-item"><i class="flaticon-home-fill-1 mr-1"></i>LOCAL</a>
                    <a href="javascript:void(0);" class="dropdown-item"><i class="flaticon-gear-fill mr-1"></i>Firebase</a>
                    <a href="javascript:void(0);" class="dropdown-item"><i class="flaticon-gear-fill mr-1"></i>S3</a>
    
                </div>
            </div>
            
            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please fill the name
            </div>
        </div>
    
    </div>
    
    </div>'''

def saveFormQuestion():
    pass


'''
    <div class="row">
        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
            <div class="form-row">
                <div class="col-md-12 mb-4">
                    <label for="fullName">Pregunta abierta</label>
                    <input type="hidden" name="questions[0]['type'(openQuestion, multi, file)]">
                    <input type="text" name="questions[0]['question']" class="form-control" id="PreguntaAbierta">
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                    <div class="invalid-feedback">
                        Please fill the name
                    </div>
                </div>
            </div>
        </div>
    </div> '''
