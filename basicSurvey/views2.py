from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'basicSurvey/survey.html')


def question(request):
    return render(request, 'questions/questions.html')