from django.db import models
from django.contrib.postgres.fields import JSONField
# Create your models here.



class FormInputType(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=150, blank=True, null=True)
    classHandler = models.CharField(max_length=50, blank=True, null=True)
    dataJson = JSONField(null = True, blank = True)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)

    updatedAt = models.DateField(blank=True, null=True)
    updatedBy = models.CharField(max_length=50, blank=True, null=True)

    deletedAt = models.DateField(blank=True, null=True)
    deletedBy = models.CharField(max_length=50, blank=True, null=True)
    
    
    
class SystemLogs(models.Model):
    entityType = models.CharField(max_length=50, blank=True, null=True)

     #entityId
    action = models.CharField(max_length=50, blank=True, null=True)
    message = models.CharField(max_length=150, blank=True, null=True)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)


class Files(models.Model):
    # (jpg, xml, json, png); "TYPE es un palabra reservada"
    type = models.CharField(max_length=50, blank=True, null=True)
    storage = models.CharField(max_length=50, blank=True, null=True)
    patch = models.CharField(max_length=150, blank=True, null=True)
    entityType = models.CharField(max_length=50, blank=True, null=True)
    #entityId
    # ( upload / created ) 
    origin  = models.CharField(max_length=50, blank=True, null=True)
    dataJson = JSONField(null = True, blank = True)
    # (system, user, cron, worker)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt= models.DateField(blank=True, null=True)
    createdBy= models.CharField(max_length=50, blank=True, null=True)


class SurveyForms(models.Model):
    version = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)

     #statusId

    startDate = models.DateField(blank=True, null=True)
    endDate = models.DateField(blank=True, null=True)
    public = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    formJson = JSONField(null = True, blank = True)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)

    updatedAt = models.DateField(blank=True, null=True)
    updatedBy = models.CharField(max_length=50, blank=True, null=True)

    deletedAt = models.DateField(blank=True, null=True)
    deletedBy = models.CharField(max_length=50, blank=True, null=True)


class SurveyAnswers(models.Model):
    surveyFormId = models.ForeignKey(SurveyForms,on_delete=models.CASCADE)
    surveyJson = JSONField(null = True, blank = True)
    creatorType = models.CharField(max_length=50, blank=True, null=True)
    createdAt = models.DateField(blank=True, null=True)
    createdBy = models.CharField(max_length=50, blank=True, null=True)
    updatedAt = models.DateField(blank=True, null=True)
    updatedBy = models.CharField(max_length=50, blank=True, null=True)
    deletedAt = models.DateField(blank=True, null=True)
    deletedBy = models.CharField(max_length=50, blank=True, null=True)
